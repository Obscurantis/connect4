#include "grid.h"

#include <stdlib.h>
#include <stdbool.h>

#include "matrix.h"

struct Grid
{
  int x, y;
  Token* t;
};

Grid* grid_new(unsigned int x, unsigned int y)
{
  Grid* g = malloc(sizeof(*g));
  g->x = x;
  g->y = y;
  g->t = malloc(x*y*sizeof(*(g->t)));
  grid_reset(g);
  return g;
}

void grid_free(Grid* g)
{
  free(g->t);
  free(g);
}

void grid_reset(Grid* g)
{
  for(int i = 0; i < g->x * g->y; i++)
  {
    *(g->t+i) = NONE;
  }
}

inline unsigned int grid_x(Grid* g)
{
  return g->x;
}

inline unsigned int grid_y(Grid* g)
{
  return g->y;
}

Token grid_geti(Grid* g, unsigned int i)
{
  if(i < g->x * g->y)
    return *(g->t + i);

  return NONE;
}

void grid_seti(Grid* g, unsigned int i, Token token)
{
  if(i < g->x * g->y)
    *(g->t + i) = token;
}

bool grid_play(Grid* g, unsigned int column, Token token)
{
  if(column >= g->x)
    return false;

  for(int i = column * g->y; i < (column + 1) * g->y; i++)
  {
    if(grid_geti(g, i) == NONE)
    {
      grid_seti(g, i, token);
      return true;
    }
  }

  return false;
}

inline Token grid_get(Grid* g, unsigned int x, unsigned int y)
{
  return grid_geti(g, x * g->y + y);
}

// helper macro for grid_check.
#define GRID_LKUP(g,s,sx,sy) \
  matrix_lookup((int*)g->t, g->x, g->y, s, sx, sy, NONE)

int grid_check(Grid* g)
{
  // linear patterns.
  int lr[] = {RED,RED,RED,RED};
  int lb[] = {BLUE,BLUE,BLUE,BLUE};

  // RED diag patterns.
  int dr1[] = { RED,0,0,0,
                0,RED,0,0,
                0,0,RED,0,
                0,0,0,RED };
  int dr2[] = { 0,0,0,RED,
                0,0,RED,0,
                0,RED,0,0,
                RED,0,0,0 };

  // BLUE diag patterns.
  int db1[] = { BLUE,0,0,0,
                0,BLUE,0,0,
                0,0,BLUE,0,
                0,0,0,BLUE };
  int db2[] = { 0,0,0,BLUE,
                0,0,BLUE,0,
                0,BLUE,0,0,
                BLUE,0,0,0 }; 

  // tests.
  if(GRID_LKUP(g, lr, 4, 1) || GRID_LKUP(g, lr, 1, 4) ||
      GRID_LKUP(g, dr1, 4, 4) || GRID_LKUP(g, dr2, 4, 4))
    return (int)RED;
    
  if(GRID_LKUP(g, lb, 4, 1) || GRID_LKUP(g, lb, 1, 4) ||
      GRID_LKUP(g, db1, 4, 4) || GRID_LKUP(g, db2, 4, 4))
    return (int)BLUE;

  if(grid_is_full(g))
    return (int)NONE;

  return -1;
}

bool grid_is_full(Grid* g)
{
  int chksum = 0;

  for(int x = 0; x < g->x; x++)
  {
    for(int y = 0; y < g->y; y++)
    {
      if(grid_get(g, x, y) != NONE)
        chksum++;
    }
  }

  if(chksum == g->x * g->y)
    return true;

  return false;
}

