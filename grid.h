#ifndef H_GRID
#define H_GRID

#include <stdbool.h>

/* A grid :
 * |
 * |
 * y
 * 0 x - - -
 */

typedef struct Grid Grid;
typedef enum Token { NONE, RED, BLUE } Token;

Grid* grid_new(unsigned int x, unsigned int y);
void grid_free(Grid* g);
void grid_reset(Grid* g);
unsigned int grid_x(Grid* g);
unsigned int grid_y(Grid* g);
bool grid_play(Grid* g, unsigned int column, Token token);
Token grid_get(Grid* g, unsigned int x, unsigned int y);
int grid_check(Grid* g);
bool grid_is_full(Grid* g);

#endif

