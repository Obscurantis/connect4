#include "display.h"

#include <string.h>
#include <ncurses.h>

#include "config.h"

void display_init(void)
{
  initscr();
  cbreak();
  noecho();
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
  curs_set(0);
}

void display_close(void)
{
  endwin();
}

void print_centered(const char* str, int vshift)
{
  int x, y;
  getmaxyx(stdscr, y, x);
  move(y/2 + vshift, (x - strlen(str))/2);
  printw(str);
}

void display_intro(void)
{
  clear();

  print_centered(INTRO, 0);
  print_centered("Press any key to continue...", 2);

  refresh();

  getch();
}

void display_grid(Grid* g, Token curtok)
{
  int x_offset = X_OFFSET;
  int y_offset = Y_OFFSET;
  Token t;

  clear();

  // display the grid.
  for(int y = 0; y < grid_y(g); y++)
  {
    move(y_offset + y, x_offset);
    printw("| ");
    for(int x = 0; x < grid_x(g); x++)
    {
      t = grid_get(g, x, grid_y(g) - y - 1);
      if(t == RED)
        printw("o ");
      else if(t == BLUE)
        printw("x ");
      else
        printw("  ");
    }
    printw("|");
  }

  // display commands.
  int size = strlen(COMMANDS);
  move(y_offset + grid_y(g), x_offset);
  printw("  ");
  for(int x = 0; x < grid_x(g) && x < size; x++)
  {
    printw("%c ", COMMANDS[x]);
  }

  // infos.
  move(y_offset + grid_y(g) + 2, x_offset + 2);
  printw("%c to quit", CMD_QUIT);

  // display current player.
  move(y_offset + grid_y(g) + 4, x_offset + 2);
  printw("%s player's turn", curtok == RED ? "RED `o`" : "BLUE `x`");

  refresh();
}

void display_game_over(Token t)
{
  clear();
  
  if (t == RED)
    print_centered("RED `o` wins !", 0);
  else if (t == BLUE)
    print_centered("BLUE `x` wins !", 0);
  else
    print_centered("You're losers !", 0);

  print_centered("Press any key to continue...", 2);

  refresh();

  getch();
}

