#ifndef H_MATRIX
#define H_MATRIX

#include <stdbool.h>

/* Look for a submatrix in a matrix, alpha is a "transparent" value. */
bool matrix_lookup(int* matrix, unsigned int x, unsigned int y, int* sub,
    unsigned int sx, unsigned int sy, int alpha);

#endif

