EXE=connect4
SOURCES=main.c display.c grid.c matrix.c
DEBUG=-g -O0

CFLAGS=-Wall -c -std=c99 $(DEBUG)
LDFLAGS=-Wall -lncurses
CC=gcc
RM=rm -rf
OBJECTS=$(SOURCES:.c=.o)

all: $(SOURCES) $(EXE)

.c.o:
	$(CC) $(CFLAGS) $< -o $@

$(EXE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

clean:
	$(RM) *.o $(EXE)

rebuild: clean all

