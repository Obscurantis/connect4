#ifndef H_INIT
#define H_INIT

#include "grid.h"

void display_init(void);
void display_close(void);
void display_intro(void);
void display_grid(Grid* g, Token curtok);
void display_game_over(Token t);

#endif

