#ifndef H_CONFIG
#define H_CONFIG

#define INTRO "A simple Connect 4 game"
#define GRID_X 7
#define GRID_Y 6
#define COMMANDS "azertyuiop"
#define X_OFFSET 2
#define Y_OFFSET 1
#define CMD_QUIT 'q'

#endif

