#include "matrix.h"

#include <stdbool.h>

bool matrix_lookup(int* matrix, unsigned int x, unsigned int y, int* sub,
    unsigned int sx, unsigned int sy, int alpha)
{
  if(sx > x || sy > y)
    return false;

  int chksum, *elem;

  for(int i = 0; i < x; i++)
  {
    for(int j = 0; j < y; j++)
    {
      chksum = 0;

      // look up for submatrix match.
      for(int k = 0; k < sx && k+i < x; k++)
      {
        for(int l = 0; l < sy && l+j < y; l++)
        {
          // current submatrix element.
          elem = sub+k*sy+l;
          // current matrix element == elem.
          if(*(matrix+(i+k)*y+(j+l)) == *elem || *elem == alpha)
            chksum++;
        }
      }
      
      // submatrix match.
      if(chksum == sx*sy)
        return true;
    }
  }

  return false;
}

