#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ncurses.h>

#include "config.h"
#include "grid.h"
#include "display.h"

int main(int argc, char* argv[])
{
  Grid* g = grid_new(GRID_X, GRID_Y);

  display_init();

  display_intro();

  // game loop.
  bool quit = false;
  Token curtok = RED;
  while(!quit)
  {
    display_grid(g, curtok);
    
    // key events.
    do
    {
      int cmd = getch();
      if(CMD_QUIT == cmd)
      {
        quit = true;
        break;
      }
      // does the command exist ?
      char* pos = strchr(COMMANDS, (char)cmd);
      if(pos == NULL)
      {
        continue;
      }
      else
      {
        // get column number.
        int column = pos - COMMANDS;
        if(grid_play(g, column, curtok))
          break;
      }
    }
    while(1);

    // victory conditions.
    int chk = grid_check(g);
    if(chk != -1)
    {
      display_game_over(chk);
      break;
    }
    
    // swap player.
    curtok = (curtok == RED) ? BLUE : RED;
  }

  display_close();
  grid_free(g);

  return EXIT_SUCCESS;
}

